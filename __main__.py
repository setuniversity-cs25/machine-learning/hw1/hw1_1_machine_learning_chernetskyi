import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.datasets import load_wine
from sklearn.preprocessing import StandardScaler

wine_dataset = load_wine()
print(wine_dataset.keys())

df = pd.DataFrame(data=wine_dataset.data, columns=wine_dataset.feature_names)

df['target'] = wine_dataset.target
print(df)

# Investigate data types, number of features, target and its distribution
print("Data Types:")
print(df.dtypes)
print("\nNumber of Features:", len(df.columns) - 1)
print("\nTarget Variable:")
print(df['target'].value_counts())

# Plot histograms (quantity per value)
df.hist(figsize=(12, 10))
plt.tight_layout()
plt.show()

# Look for correlation of features (with target)
correlation_matrix = df.corr()
plt.figure(figsize=(12, 8))
sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt=".2f")
plt.title('Correlation Heatmap')
plt.show()

# Encode and/or normalize (scale) the features if necessary
# As all data are numeric, we do not need encoding
# Scaling the features using StandardScaler
scaler = StandardScaler()
df_scaled = pd.DataFrame(scaler.fit_transform(df.drop('target', axis=1)), columns=df.columns[:-1])

# Look for anomalies: sample with > 3*sigma (std.) from the mean
anomalies = df[np.abs(df - df.mean()) > 3 * df.std()].dropna()
print("\nAnomalies:")
print(anomalies)

# Save scaled data to csv
df_scaled.to_csv('df_EDA.csv', index=False)
